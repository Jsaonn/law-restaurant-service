from django.urls import path
from . import views

app_name = 'order_food'
urlpatterns = [
    path('', views.get_order_list, name="orderView"),
    path('<int:pk>/', views.get_order_detail, name="orderViewWithId"),
    path('create/', views.create_order_object, name="orderCreate"),
    path('food/', views.get_order_food_list, name="orderFoodView"),
]