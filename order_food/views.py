from django.views.decorators.http import require_http_methods
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema

from .serializers import OrderObjectSerializer, OrderFoodSerializer, OrderObjectFoodSerializer
from .models import OrderObject, OrderFood
from restaurant_data.models import RestaurantData, MenuData

import os
import json
import requests

@require_http_methods(['GET'])
@api_view(['GET'])
def get_order_list(request):
    order_list = OrderObject.objects.all()
    serializer = OrderObjectFoodSerializer(order_list, many=True)
    send_logs('get_order_list', {}, '', '', serializer)
    return Response(
        {"data": serializer.data}
    )

@require_http_methods(['GET'])
@api_view(['GET'])
def get_order_detail(request, pk):
    order_object = get_object_or_404(OrderObject, pk=pk)
    serializer = OrderObjectFoodSerializer(order_object)
    send_logs('get_order_detail', {}, '', pk, serializer)
    return Response(
        {"data": serializer.data}
    )

@swagger_auto_schema(methods=['post'], request_body=OrderObjectSerializer)
@require_http_methods(['POST'])
@api_view(['POST'])
def create_order_object(request, *args, **kwargs):
    restaurant = RestaurantData.objects.get(pk=request.data['restaurant_id'])
    data = {
        'order_service': request.data['order_id'],
        'restaurant': restaurant.name
    }
    print(request.data)
    print(request.data['menu_id_list'])
    serializer = OrderObjectSerializer(data=data)
    if serializer.is_valid():
        serializer.save()
        print(request.data['menu_id_list'])
        create_order_food(serializer.data['id'], request.data['menu_id_list'])
        send_logs('create_order_object', data, '', '', serializer)
        # print(serializer.data)
        return Response(
            {"data": serializer.data},
            status=status.HTTP_201_CREATED
        )
    else:
        return Response(
            {"error": serializer.errors},
            status=status.HTTP_400_BAD_REQUEST
        )


@require_http_methods(['GET'])
@api_view(['GET'])
def get_order_food_list(request):
    order_list = OrderFood.objects.all()
    serializer = OrderFoodSerializer(order_list, many=True)
    send_logs('get_order_food_list', {}, '', '', serializer)
    return Response(
        {"data": serializer.data}
    )

# -----------------------------------------------------------------

def create_order_food(id_order, menu_list):
    # print(menu_list)
    for id_menu in menu_list:
        menu = MenuData.objects.get(pk=id_menu)
        data = {
            'order': id_order,
            'name': menu.name,
        }
        serializer = OrderFoodSerializer(data=data)
        if serializer.is_valid():
            serializer.save()

# ------------------------------------------------------------------

BASE_URL = "https://restaurant-service-law.herokuapp.com/order-food"
LOG_URL = "https://logs-01.loggly.com/inputs/c33818a3-eb2d-4b4f-8d89-6dae5e3993c1/tag/http"

VIEWS_URL_MAP = {
    'get_order_list': {
        'url': '',
        'method': 'GET'
    },
    'get_order_detail': {
        'url': '',
        'method': 'GET'
    },
    'create_order_object': {
        'url': 'create/',
        'method': 'POST'
    },
    'get_order_food_list': {
        'url': 'food/',
        'method': 'GET'
    }
}

def send_logs(view_name, payload, additional_headers, additional_url_path, response):
    if os.environ.get('IS_PRODUCTION_ENV') == 'False':
        return

    method = VIEWS_URL_MAP[view_name]["method"]
    url = VIEWS_URL_MAP[view_name]["url"]

    payload = {
        'service': 'ORDER_SERVICE',
        'method': method,
        'endpoint': f"{BASE_URL}/{url}{additional_url_path}",
        'payload': payload,
        'additional_headers': additional_headers,
        'response_data': response.data,
    }
    print('-----------------------------')
    print('log payload:',payload)

    result = requests.post(LOG_URL, data=json.dumps(payload))
    if result.status_code != 200:
        print("Order service failed sending logs :(")

    print("Order service successfully sending logs!")