from apscheduler.schedulers.background import BackgroundScheduler
from order_food.models import OrderObject, OrderFood
from order_food.serializers import OrderFoodSerializer
from datetime import datetime, timedelta

import pika


params = pika.URLParameters("amqp://nlekkhuz:8uhhFH6MhUL0LIZA3ijM6y1QMe7k3EbA@albatross.rmq.cloudamqp.com/nlekkhuz")
connection = pika.BlockingConnection(params)
channel = connection.channel()

channel.queue_declare(queue='order_status_update')


def check_order_ready():
    today = datetime.now().strftime('%H:%M:%S')
    orders = OrderObject.objects.filter(is_ready_cook=False)
    for order in orders:
        ready_time = order.created_at + timedelta(minutes=1)
        if ready_time.strftime('%H:%M:%S') < today:
            order.is_ready_cook = True
            order.status_order = 'Pesanan dimasak'
            order.save()
            now = datetime.now().strftime('%H:%M:%S')
            message = f"{order.order_service}-Pesanan dimasak-{now}"
            channel.basic_publish(exchange='', routing_key='order_status_update', body=message)
            print(f" [x] Sent {message}")

def cooking_process():
    today = datetime.now().strftime('%H:%M:%S')
    orders = OrderObject.objects.filter(is_ready_cook=True, status_order='Pesanan dimasak')
    for order in orders:
        menus = OrderFood.objects.filter(is_done=False, order=order.id)
        for menu in menus:
            ready_time = menu.created_at + timedelta(minutes=5)
            if ready_time.strftime('%H:%M:%S') < today:
                menu.is_done = True
                menu.save()
            check_order_done(order)

def check_order_done(order):
    count = 0
    menus = OrderFood.objects.filter(order=order.id)
    for menu in menus:
        if menu.is_done == False:
            count += 1

    if count == 0:
        order.status_order = 'Pesanan selesai'
        order.save()
        now = datetime.now().strftime('%H:%M:%S')
        message = f"{order.order_service}-Pesanan selesai-{now}"
        channel.basic_publish(exchange='', routing_key='order_status_update', body=message)
        print(f" [x] Sent {message}")

def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(check_order_ready, "interval", minutes=1, id="order_001", replace_existing=True)
    scheduler.add_job(cooking_process, "interval", minutes=1, id="order_002", replace_existing=True)
    scheduler.start()
