from django.contrib import admin
from .models import OrderObject, OrderFood

# Register your models here.
admin.site.register(OrderObject)
admin.site.register(OrderFood)