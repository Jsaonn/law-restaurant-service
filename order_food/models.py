from django.db import models
from datetime import datetime


status_choices = (
    ('Pesanan diterima', 'Pesanan diterima'),
    ('Pesanan dimasak', 'Pesanan dimasak'),
    ('Pesanan selesai', 'Pesanan selesai')
)


class OrderObject(models.Model):
    order_service = models.IntegerField(default=0)
    restaurant = models.CharField(
        max_length=255
    )
    status_order = models.CharField(
        max_length=255,
        choices=status_choices,
        default="Pesanan diterima"
    )
    created_at = models.DateTimeField(
        default=datetime.now()
    )
    is_ready_cook = models.BooleanField(
        default=False
    )

    def __str__(self) -> str:
        return '{} - {}'.format(self.restaurant, self.status_order)


class OrderFood(models.Model):
    order = models.ForeignKey(
        to=OrderObject, 
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="orderFood"
    )
    name = models.CharField(
        max_length=255
    )
    created_at = models.DateTimeField(
        default=datetime.now()
    )
    is_done = models.BooleanField(
        default=False
    )

    def __str__(self) -> str:
        return '{} - {}'.format(self.name, self.is_done)
