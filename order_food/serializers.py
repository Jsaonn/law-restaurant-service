from rest_framework import serializers
from .models import OrderObject, OrderFood


class OrderObjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderObject
        fields = '__all__'

class OrderFoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderFood
        fields = '__all__'

class OrderObjectFoodSerializer(serializers.ModelSerializer):
    orderFood = OrderFoodSerializer(many=True)

    class Meta:
        model = OrderObject
        fields = (
            'id',
            'order_service',
            'restaurant',
            'status_order',
            'created_at',
            'is_ready_cook',
            'orderFood',
        )
