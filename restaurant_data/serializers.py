from rest_framework import serializers
from .models import RestaurantData, MenuData


class MenuDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuData
        fields = '__all__'

class RestaurantDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = RestaurantData
        fields = '__all__'

class RestaurantMenuDataSerializer(serializers.ModelSerializer):
    menuData = MenuDataSerializer(many=True)

    class Meta:
        model = RestaurantData
        fields = (
            'id',
            'name',
            'address',
            'open_time',
            'close_time',
            'telp_num',
            'rating',
            'menuData',
        )