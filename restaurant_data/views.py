from django.views.decorators.http import require_http_methods
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema

from .serializers import MenuDataSerializer, RestaurantDataSerializer, RestaurantMenuDataSerializer
from .models import RestaurantData, MenuData

from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.core.cache import cache

import os
import json
import requests


CACHE_TTL  = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@require_http_methods(['GET'])
@api_view(['GET'])
def get_restaurant_list(request):
    if cache.get('resto'):
        print('DATA CACHE')
        restaurant_list = cache.get('resto')
    else:
        print('DATA DB')
        restaurant_list = RestaurantData.objects.all()
        cache.set('resto', restaurant_list, timeout=(60*5))
    
    serializer = RestaurantDataSerializer(restaurant_list, many=True)
    send_logs('get_restaurant_list', {}, '', '', serializer)
    return Response(
        {"data": serializer.data}
    )

@require_http_methods(['GET'])
@api_view(['GET'])
def get_restaurant_detail(request, pk):
    if cache.get(pk):
        print('DATA CACHE')
        restaurant = cache.get(pk)
    else :
        print('DATA DB')
        restaurant = get_object_or_404(RestaurantData, pk=pk)
        cache.set(pk, restaurant, timeout=(60*5))
        
    serializer = RestaurantMenuDataSerializer(restaurant)
    send_logs('get_restaurant_detail', {}, '', pk, serializer)
    return Response(
        {"data": serializer.data}
    )

@swagger_auto_schema(methods=['post'], request_body=RestaurantDataSerializer)
@require_http_methods(['POST'])
@api_view(['POST'])
def create_restaurant(request, *args, **kwargs):
    serializer = RestaurantDataSerializer(data=request.data)
    if serializer.is_valid():
        open_time = serializer.validated_data['open_time']
        close_time = serializer.validated_data['close_time']
        if open_time >= close_time:
            return Response(
                {"error": "open time always before close time"},
                status=status.HTTP_400_BAD_REQUEST
            )
        serializer.save()
        return Response(
            {"data": serializer.data},
            status=status.HTTP_201_CREATED
        )
    else:
        return Response(
            {"error": serializer.errors},
            status=status.HTTP_400_BAD_REQUEST
        )

@swagger_auto_schema(methods=['put'], request_body=RestaurantDataSerializer)
@require_http_methods(['PUT'])
@api_view(['PUT'])
def update_restaurant(request, pk):
    restaurant = get_object_or_404(RestaurantData, pk=pk)
    serializer = RestaurantDataSerializer(restaurant, data=request.data)
    if serializer.is_valid():
        open_time = serializer.validated_data['open_time']
        close_time = serializer.validated_data['close_time']
        if open_time >= close_time:
            return Response(
                {"error": "open time always before close time"},
                status=status.HTTP_400_BAD_REQUEST
            )
        serializer.save()
        return Response(
            {"data": serializer.data},
            status=status.HTTP_200_OK
        )
    else:
        return Response(
            {"error": serializer.errors},
            status=status.HTTP_400_BAD_REQUEST
        )

@require_http_methods(['DELETE'])
@api_view(['DELETE'])
def delete_restaurant(request, pk):
    restaurant = get_object_or_404(RestaurantData, pk=pk)
    restaurant.delete()
    return Response({"data": "Item Deleted!"})


# ---------------------------------------------------------------


@require_http_methods(['GET'])
@api_view(['GET'])
def get_menu_list(request):
    menu_list = MenuData.objects.all()
    serializer = MenuDataSerializer(menu_list, many=True)
    return Response(
        {"data": serializer.data}
    )

@require_http_methods(['GET'])
@api_view(['GET'])
def get_menu_detail(request, pk):
    menu = get_object_or_404(MenuData, pk=pk)
    serializer = MenuDataSerializer(menu)
    send_logs('get_menu_detail', {}, '', pk, serializer)
    return Response(
        {"data": serializer.data}
    )

@swagger_auto_schema(methods=['post'], request_body=MenuDataSerializer)
@require_http_methods(['POST'])
@api_view(['POST'])
def create_menu(request, *args, **kwargs):
    serializer = MenuDataSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(
            {"data": serializer.data},
            status=status.HTTP_201_CREATED
        )
    else:
        return Response(
            {"error": serializer.errors},
            status=status.HTTP_400_BAD_REQUEST
        )

@swagger_auto_schema(methods=['put'], request_body=MenuDataSerializer)
@require_http_methods(['PUT'])
@api_view(['PUT'])
def update_menu(request, pk):
    menu = get_object_or_404(MenuData, pk=pk)
    serializer = MenuDataSerializer(menu, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(
            {"data": serializer.data},
            status=HTTP_200_OK
        )
    else:
        return Response(
            {"error": serializer.errors},
            status=status.HTTP_400_BAD_REQUEST
        )

@require_http_methods(['DELETE'])
@api_view(['DELETE'])
def delete_menu(request, pk):
    menu = get_object_or_404(MenuData, pk=pk)
    menu.delete()
    return Response({"data": "Item Deleted!"})


# ---------------------------------------------------------------

BASE_URL = "https://restaurant-service-law.herokuapp.com/restaurant-data"
LOG_URL = "https://logs-01.loggly.com/inputs/c33818a3-eb2d-4b4f-8d89-6dae5e3993c1/tag/http"

VIEWS_URL_MAP = {
    'get_restaurant_list': {
        'url': '',
        'method': 'GET'
    },
    'get_restaurant_detail': {
        'url': '',
        'method': 'GET'
    },
    'get_menu_detail': {
        'url': 'menu/',
        'method': 'POST'
    }
}

def send_logs(view_name, payload, additional_headers, additional_url_path, response):
    if os.environ.get('IS_PRODUCTION_ENV') == 'False':
        return

    method = VIEWS_URL_MAP[view_name]["method"]
    url = VIEWS_URL_MAP[view_name]["url"]

    payload = {
        'service': 'RESTAURANT_SERVICE',
        'method': method,
        'endpoint': f"{BASE_URL}/{url}{additional_url_path}",
        'payload': payload,
        'additional_headers': additional_headers,
        'response_data': response.data,
    }
    print('log payload:',payload)

    result = requests.post(LOG_URL, data=json.dumps(payload))
    if result.status_code != 200:
        print("Restaurant service failed sending logs :(")

    print("Restaurant service successfully sending logs!")