from django.urls import path
from . import views

app_name = 'restaurant_data'
urlpatterns = [
    path('', views.get_restaurant_list, name="restaurantView"),
    path('<int:pk>/', views.get_restaurant_detail, name="restaurantViewWithId"),
    path('create/', views.create_restaurant, name="restaurantCreate"),
    path('update/<int:pk>/', views.update_restaurant, name="restaurantUpdate"),
    path('delete/<int:pk>/', views.delete_restaurant, name="restaurantDelete"),
    # -------------------------------
    path('menu/', views.get_menu_list, name="menuView"),
    path('menu/<int:pk>/', views.get_menu_detail, name="menuViewWithId"),
    path('menu/create/', views.create_menu, name="menuCreate"),
    path('menu/update/<int:pk>/', views.update_menu, name="menuUpdate"),
    path('menu/delete/<int:pk>/', views.delete_menu, name="menuDelete"),
]