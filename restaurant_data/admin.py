from django.contrib import admin
from .models import RestaurantData, MenuData

# Register your models here.
admin.site.register(RestaurantData)
admin.site.register(MenuData)