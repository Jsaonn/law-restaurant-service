from django.db import models


class RestaurantData(models.Model):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    open_time = models.TimeField()
    close_time = models.TimeField()
    telp_num = models.IntegerField()
    rating = models.IntegerField()

    def __str__(self) -> str:
        return '{} - {}'.format(self.name, self.telp_num)


class MenuData(models.Model):
    restaurant = models.ForeignKey(RestaurantData, on_delete=models.CASCADE, related_name="menuData")
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    price = models.IntegerField()

    def __str__(self) -> str:
        return 'Restaurant {} - {} ({})'.format(str(self.restaurant.name), self.name, self.price)
